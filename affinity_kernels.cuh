#ifndef __AFFINITY_KERNELS_CUH__
#define __AFFINITY_KERNELS_CUH__

#include <thrust/functional.h>

#include <transpose.cuh>

typedef tuple<int, float, float> Tuple_iff;
typedef tuple<int, float> Tuple_if;

struct max_ind : public thrust::binary_function<int, int, int> {
	const int size, i;
	const float* s;

	max_ind(int _size, int _i, float* _s) : s(_s), size(_size), i(_i) {}

	__host__ __device__
		int operator()(int t, int m) {
			if (s[t*size + i] > s[m*size + i])
				return t;
			else
				return m;
		}
};

struct calculate_similarity : public thrust::unary_function<float, int> {
	/* Calculate Negative Squared Euclidean Distance */
	const int size;
	const float SELF_SIMILARITY;
	const float *x, *y, *z;

	calculate_similarity(int _size, float _SS, float* _x, float* _y, float* _z) : size(_size), x(_x), y(_y), z(_z), SELF_SIMILARITY(_SS) {}

	__host__ __device__
		float operator()(int v) {
			int v1 = v % size;
			int v2 = v / size;

			float dx = x[v1] - x[v2];
			float dy = y[v1] - y[v2];
			float dz = z[v1] - z[v2];

			float negSqED = -(dx*dx + dy*dy + dz*dz);

			if (v1 == v2)
				negSqED = SELF_SIMILARITY;

			return negSqED;
		}
};

__global__ void calculate_sums_kernel(const int N, float* resp, float* row_sums) {
	register int k = threadIdx.y + blockDim.y*blockIdx.y;
	register float sum = 0;

	for (int i = 0; i < N; i++) {
		register float val = fmaxf(0.0f, resp[i*N + k]);
		sum += val;
	}

	row_sums[k] = sum;
}

__global__ void calculate_maxima_kernel(const int N, float* sums, Tuple_iff* results) {
	register int i = threadIdx.y + blockDim.y*blockIdx.y;
	register int max = 0;
	register float m1 = -1e100, m2 = -1e100;

	for (int k = 0; k < N; k++) {
		register float val = sums[k*N + i];
		if (val > m1) {
			m2 = m1; m1 = val; max = k;
		}
		else if (val > m2) {
			m2 = val;
		}
	}

	results[i] = make_tuple(max, m1, m2);
}

void calculate_grid(const int N, dim3* block, dim3* thread) {
	if(N <= 64) { // We are working with a very small data set
		*block = dim3(1, 1);
		*thread = dim3(1, N);
	} else if(N%512 == 0 && N >= 1024) { //Otherwise allocate larger thread blocks, such that there are at least 2 blocks running simultaneously. 
		*block = dim3(1, N/512);
		*thread = dim3(1, 512);
	} else if(N%256 == 0 && N >= 512) {
		*block = dim3(1, N/256);
		*thread = dim3(1, 256);
	} else if(N%128 == 0 && N >= 256) {
		*block = dim3(1, N/128);
		*thread = dim3(1, 128);
	} else if(N%64 == 0 && N >= 128) {
		*block = dim3(1, N/64);
		*thread = dim3(1, 64);
	} else if(N%32 == 0 && N >= 64) {
		*block = dim3(1, N/32);
		*thread = dim3(1, 32);
	} else {
		printf("Not Implemented");
		exit(-1);
	}
}

void calculate_sums(const int N, float* resp, float* temp, float* row_sums) {
	dim3 block, thread;
	calculate_grid(N, &block, &thread);

	transposeInto(resp, temp, N);
	
	calculate_sums_kernel<<<block, thread>>>(N, temp, row_sums);

}

void calculate_maxima(const int N, float* sums, Tuple_iff* results) {
	dim3 block, thread;
	calculate_grid(N, &block, &thread);

	calculate_maxima_kernel<<<block, thread>>>(N, sums, results);

}

struct calculate_exemplars : public thrust::binary_function<Tuple_if, Tuple_if, Tuple_if> {

	__host__ __device__
		Tuple_if operator()(Tuple_if &i, Tuple_if &j) {
			Tuple_if out;
			if(get<1>(i) > get<1>(j))
				return i;
			else
				return j;
		}
};

struct calculate_responsibility : public thrust::unary_function<float, tuple<int, Tuple_iff&> > {
	/* Calculate updated responsibility */

	const int size;
	const float DAMPING_FACTOR;
	const float *s, *r, *a, *su;

	calculate_responsibility(int _size, float DF, float* _s, float* _r, float* _a, float* _su) : size(_size), DAMPING_FACTOR(DF), s(_s), r(_r), a(_a), su(_su) {}

	__host__ __device__
		float operator()(tuple<int, Tuple_iff& > &t) {
			int ik = get<0>(t);
			int k = ik / size;

			tuple<int, float, float> m = get<1>(t);

			float max = -1e100;
			
			int ind = m.get<0>();

			if(k == ind)
				max = m.get<2>();
			else
				max = m.get<1>();

			return (r[ik]*(1-DAMPING_FACTOR)) + ((s[ik] - max)*DAMPING_FACTOR);
		}
};

struct calculate_availability : public thrust::unary_function<float, int> {
	/* Calculate updated availability */

	const int size;
	const float DAMPING_FACTOR;
	const float *s, *r, *a, *rm;

	calculate_availability(int _size, float DF, float* _s, float* _r, float* _a, float* _rm) : size(_size), DAMPING_FACTOR(DF), s(_s), r(_r), a(_a), rm(_rm) {}

	__host__ __device__
		float operator()(int ik) {
			int i = ik % size;
			int k = ik / size;
	
			float sum = 0;
			if(i == k) {
				sum = rm[k] - fmaxf(0, r[i + k*size]);
				return a[ik]*(1-DAMPING_FACTOR) + sum*(DAMPING_FACTOR);
			}
			else {
				sum = rm[k] - fmaxf(0, r[i + k*size]) - fmaxf(0, r[k + k*size]);
				return a[ik]*(1-DAMPING_FACTOR) + fminf(0, r[k+k*size] + sum)*DAMPING_FACTOR;
			}
		}
};

#endif
