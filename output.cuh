#ifndef __OUTPUT_CUH__
#define __OUTPUT_CUH__

#include <thrust/version.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

using namespace thrust;

void print_matrix(int size, host_vector<float> m);
float generateColour(const int size, int id);
void output_pcd(const int size, host_vector<float> x, host_vector<float> y, host_vector<float> z, host_vector<tuple<int, float> > exemplars);

#endif
