NVCC=nvcc
NVCC_CFLAGS=-g -I. -O3 -DBOOST_LCAST_NO_COMPILE_TIME_PRECISION 
NVCC_LDFLAGS=-lboost_program_options

BINARY=affinity

.PHONY: all clean 

all: $(BINARY)

%.o: %.cu
	$(NVCC) $(NVCC_CFLAGS) -c -o $@ $^

$(BINARY): kernel.o random.o transpose.o output.o
	$(NVCC) $(NVCC_LDFLAGS) -o $@ $^

clean:
	-rm -f $(BINARY) *.o
