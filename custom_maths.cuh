#ifndef __CUSTOM_MATH_CUH__
#define __CUSTOM_MATH_CUH__

#include <thrust/functional.h>

template <typename T>
struct cdiv : public thrust::unary_function<T, T>{
	const T m;

	cdiv(T _m) : m(_m) {}

	__host__ __device__
		T operator()(T v) {
			return v/m;
		}
};

struct cmod : public thrust::unary_function<int, int>{
	const int m;

	cmod(int _m) : m(_m) {}

	__host__ __device__
		int operator()(int v) {
			return v%m;
		}
};

struct madd : public thrust::unary_function<int, int>{
	const int m, a;

	madd(int _m, int _a) : m(_m), a(_a) {}

	__host__ __device__
		int operator()(int v) {
			return v*m + a;
		}
};

struct crot : public thrust::unary_function<int, int>{
	const int size;

	crot(int _size) : size(_size) {};

	__host__ __device__
		int operator()(int ik) {
			int i = ik%size;
			int j = ik/size;
			return i*size + j;
		}
};

struct umaxf : public thrust::unary_function<float, float> {
	const float m;
	umaxf(float _m) : m(_m) {};

	__host__ __device__
		float operator()(float x) {
			return fmaxf(m, x);
		}
};

#endif
