#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <time.h>
#include <math.h>

#include <boost/generator_iterator.hpp>
#include <boost/random.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

boost::mt19937 generator;
boost::normal_distribution<double> norm(0.0, 0.25);
boost::variate_generator<boost::mt19937&, boost::normal_distribution<double> > uni(generator, norm);

void seed_rand(int seed) {
	generator.seed(seed);
}

float rand_float() {
	float d = (float)uni();
	return d;
}
