#include <thrust/version.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <stdio.h>
#include <stdint.h>

using namespace thrust;

void print_matrix(int size, host_vector<float> m) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			float v = m[j + i*size];
			printf("%8.3f ", v);
		}
		std::cout << std::endl;
	}
}

float generateColour(int id, const int size) {
	// Use HSV Colour Space to define colours for clusters
	// Constants
	const static float s = 0.99; // Saturation
	const static float v = 0.99; // Value
	const static float c = v*s; // Chroma

	float r, g, b;

	// Calculate a hue, based on ID
	id = (id*334214467);
	id %= (int)size;
	float h = 0.618033988749895 + (float)id/size; // Golden Ratio
	h -= floor(h);
	h = floor(h*360);
	int hi = floor(h);

	float x = h/60.0f; while(x > 2) x-=2;
	x = c*(1-fabs(x-1));

	int hp = hi/60;
	switch(hp) {
		case 0:
			r = v; g = x; b = 0;
			break;
		case 1:
			r = x; g = c; b = 0;
			break;
		case 2:
			r = 0; g = c; b = x;
			break;
		case 3:
			r = 0; g = x; b = c;
			break;
		case 4:
			r = x; g = 0; b = c;
			break;
		case 5:
			r = c; g = 0; b = x;
			break;
		default:
			r = 0; g = 0; b = 0;
	}

	r += v-c; g += v-c; b += v-c;
	uint8_t ri = r*255, gi = g*255, bi = b*255;
	uint32_t rgb = ((uint32_t)ri << 16 | (uint32_t)gi << 8 | (uint32_t)bi);
	return *reinterpret_cast<float*>(&rgb);

}

void output_pcd(const int size, host_vector<float> x, host_vector<float> y, host_vector<float> z, host_vector<tuple<int, float> > exemplars) {
	const static char* header = "# .PCD v0.7 - Point Cloud Data file format\n"
		"VERSION 0.7\n"
		"FIELDS x y z rgb\n"
		"SIZE 4 4 4 4\n"
		"TYPE F F F F\n"
		"COUNT 1 1 1 1\n"
		"WIDTH 1\n"
		"HEIGHT %d\n"
		"VIEWPOINT 0 0 0 1 0 0 0\n"
		"POINTS %d\n"
		"DATA ascii\n";
	
	FILE* output = fopen("output.pcd", "w");
	fprintf(output, header, size, size);

	for(int i = 0; i < size; i++) {
		fprintf(output, "%.4f %.4f %.4f %.8e\n", x[i], y[i], z[i], generateColour(get<0>(exemplars[i]), size));
	}

	fclose(output);
}

