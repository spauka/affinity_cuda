#ifndef __TRANSPOSE_CUH__
#define __TRANSPOSE_CUH__

void transpose(float* om, int size);
void transposeInto(float* im, float* om, int size);

#endif
