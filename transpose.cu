#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "transpose.cuh"

// Transpose that effectively reorders execution of thread blocks along diagonals of the 
// matrix (also coalesced and has no bank conflicts)
//
// Here blockIdx.x is interpreted as the distance along a diagonal and blockIdx.y as 
// corresponding to different diagonals
//
// blockIdx_x and blockIdx_y expressions map the diagonal coordinates to the more commonly 
// used cartesian coordinates so that the only changes to the code from the coalesced version 
// are the calculation of the blockIdx_x and blockIdx_y and replacement of blockIdx.x and 
// bloclIdx.y with the subscripted versions in the remaining code

template <int TILE_DIM, int BLOCK_ROWS>
__global__ void transposeDiagonal(float *odata, float *idata, int width, int height)
{
	__shared__ float tile[TILE_DIM][TILE_DIM+1];

	int blockIdx_x, blockIdx_y;

	// do diagonal reordering
	if (width == height) {
		blockIdx_y = blockIdx.x;
		blockIdx_x = (blockIdx.x+blockIdx.y)%gridDim.x;
	} else {
		int bid = blockIdx.x + gridDim.x*blockIdx.y;
		blockIdx_y = bid%gridDim.y;
		blockIdx_x = ((bid/gridDim.y)+blockIdx_y)%gridDim.x;
	}    

	// from here on the code is same as previous kernel except blockIdx_x replaces blockIdx.x
	// and similarly for y

	int xIndex = blockIdx_x * TILE_DIM + threadIdx.x;
	int yIndex = blockIdx_y * TILE_DIM + threadIdx.y;  
	int index_in = xIndex + (yIndex)*width;

	xIndex = blockIdx_y * TILE_DIM + threadIdx.x;
	yIndex = blockIdx_x * TILE_DIM + threadIdx.y;
	int index_out = xIndex + (yIndex)*height;

	for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS) {
		tile[threadIdx.y+i][threadIdx.x] = idata[index_in+i*width];
	}

	__syncthreads();

	for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS) {
		odata[index_out+i*height] = tile[threadIdx.x][threadIdx.y+i];
	}
}

void transpose(float* om, int size) {
	float* im;
	cudaMalloc(&im, sizeof(float)*size*size);
	cudaMemcpy(im, om, sizeof(float)*size*size, cudaMemcpyDeviceToDevice);
	transposeInto(im, om, size);
	cudaFree(im);
}

void transposeInto(float* im, float* om, int size) {
	// Calculate a valid tile size for the copy
	int TILE_DIM, BLOCK_DIM;
	if(size%16 == 0) {
		TILE_DIM = BLOCK_DIM = 16;
		// Calculate execution parameters
		dim3 grid(size/TILE_DIM, size/TILE_DIM), threads(TILE_DIM, BLOCK_DIM);
		transposeDiagonal<16,16><<<grid, threads>>>(om, im, size, size);
	}
	else if(size%8 == 0) {
		TILE_DIM = BLOCK_DIM = 8;
		// Calculate execution parameters
		dim3 grid(size/TILE_DIM, size/TILE_DIM), threads(TILE_DIM, BLOCK_DIM);
		transposeDiagonal<8,8><<<grid, threads>>>(om, im, size, size);
	}
	else if(size%4 == 0) {
		TILE_DIM = BLOCK_DIM = 4;
		// Calculate execution parameters
		dim3 grid(size/TILE_DIM, size/TILE_DIM), threads(TILE_DIM, BLOCK_DIM);
		transposeDiagonal<4,4><<<grid, threads>>>(om, im, size, size);
	}
	else if(size%2 == 0) {
		TILE_DIM = BLOCK_DIM = 2;
		// Calculate execution parameters
		dim3 grid(size/TILE_DIM, size/TILE_DIM), threads(TILE_DIM, BLOCK_DIM);
		transposeDiagonal<2,2><<<grid, threads>>>(om, im, size, size);
	}
	else {
		TILE_DIM = BLOCK_DIM = 1;
		// Calculate execution parameters
		dim3 grid(size/TILE_DIM, size/TILE_DIM), threads(TILE_DIM, BLOCK_DIM);
		transposeDiagonal<1,1><<<grid, threads>>>(om, im, size, size);
	}
	return;
}
