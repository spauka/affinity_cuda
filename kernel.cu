#include <thrust/version.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/sort.h>

#include <boost/program_options.hpp>

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <time.h>
#include <math.h>
#include <set>

#include "random.cuh"
#include "output.cuh"
#include "transpose.cuh"
#include "custom_maths.cuh"
#include "affinity_kernels.cuh"

/*#define N 640
#define SELF_SIMILARITY -2.75
#define DAMPING_FACTOR 0.5
#define STOPPING_COND 25*/

using namespace thrust;
namespace po = boost::program_options;

void generateRandomPoints(int size, host_vector<float>* x, host_vector<float>* y, host_vector<float>* z) {
	seed_rand(42);
	for (int i = 0; i < size/4; i++) {
		(*x)[i] = rand_float();
		(*y)[i] = rand_float();
		(*z)[i] = rand_float();
	}
	for (int i = size/4; i < size/2; i++) {
		(*x)[i] = rand_float()+2;
		(*y)[i] = rand_float();
		(*z)[i] = rand_float();
	}
	for (int i = size/2; i < 3*size/4; i++) {
		(*x)[i] = rand_float();
		(*y)[i] = rand_float()+2;
		(*z)[i] = rand_float();
	}
	for (int i = 3*size/4; i < size; i++) {
		(*x)[i] = rand_float();
		(*y)[i] = rand_float();
		(*z)[i] = rand_float()+2;
	}
}

int main(int argc, char** argv) {

	// User boost options to parse command line arguments
	int N, STOPPING_COND;
	float SELF_SIMILARITY;
	float SCALING_FACT;
	float DAMPING_FACTOR;
	po::options_description desc("Parameters: ");
	desc.add_options()
		("help", "Prints out this help message")
		("num-points", po::value<int>(&N)->default_value(320), 
			"Selects the number of points to use")
		("self-similarity", po::value<float>(&SELF_SIMILARITY)->default_value(-1.0f),
			"Sets the self-similarity value to use")
		("damping-factor", po::value<float>(&DAMPING_FACTOR)->default_value(0.5f), 
			"Sets the damping-factor to use")
		("stopping-cond", po::value<int>(&STOPPING_COND)->default_value(25), 
			"Sets the number of iterations during which exemplars don't change before halting")
		("scaling-factor", po::value<float>(&SCALING_FACT)->default_value(1),
			"Scales (divides) similarities by this number")
		;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 0;
	}

	int major = THRUST_MAJOR_VERSION;
	int minor = THRUST_MINOR_VERSION;

	std::cout << "Thrust Version: " << major << "." << minor << std::endl;
	std::cout << "Parameters: " << std::endl;
	std::cout << "\tPoints: " << N << std::endl;
	std::cout << "\tStopping Cond: " << STOPPING_COND << std::endl;
	std::cout << "\tSelf Similarity: " << SELF_SIMILARITY << std::endl;
	std::cout << "\tDamping Factor: " << DAMPING_FACTOR << std::endl;

	// First initialize values on our host. These values can then be copied across
	// to the device since initialization will eventually be replaced by a file read.
	host_vector<float> x(N), y(N), z(N);

	generateRandomPoints(N, &x, &y, &z);

	device_vector<float> xd = x, yd = y, zd = z;
	// Make an N*N Device Vector flattened to 1D
	device_vector<float> distanceMatrix(N*N);
	float* sp = raw_pointer_cast(&distanceMatrix[0]);

	// Initialize a counter with the size of the array
	counting_iterator<int> first(0);
	counting_iterator<int> last = first + N*N;
	
	float* xp = raw_pointer_cast(&xd[0]);
	float* yp = raw_pointer_cast(&yd[0]);
	float* zp = raw_pointer_cast(&zd[0]);

	
	// For each of the point pairs, calculate the distance between the two points.
	thrust::transform(first, last,
			distanceMatrix.begin(),
			calculate_similarity(N, SELF_SIMILARITY, xp, yp, zp));
	// Normalize the matrix, since sometimes there seems to be huge blowouts in the values for some reason
	thrust::transform(distanceMatrix.begin(), distanceMatrix.end(), distanceMatrix.begin(), cdiv<float>(SCALING_FACT));
	
	// Start iterating for constant propogation
	int nChanged = 0;
	int nIter = 0;
	
	device_vector<float> responsibilities(N*N);
	device_vector<tuple<int, float, float> > row_maximas(N);
	device_vector<tuple<int, float> > exemplars(N);
	device_vector<float> row_sums(N);
	device_vector<float> availabilities(N*N);
	device_vector<float> sum(N*N);
	host_vector<tuple<int, float> > hexemplars(N), pexemplars(N);

	float* rp = raw_pointer_cast(&responsibilities[0]);
	float* ap = raw_pointer_cast(&availabilities[0]);
	float* sup = raw_pointer_cast(&sum[0]);
	float* rmp = raw_pointer_cast(&row_sums[0]);

	fill(responsibilities.begin(), responsibilities.end(), 0);
	fill(availabilities.begin(), availabilities.end(), 0);
	fill(exemplars.begin(), exemplars.end(), 0);
	fill(pexemplars.begin(), pexemplars.end(), 0);
	
	while(nChanged < STOPPING_COND) {

		typedef transform_iterator<cmod, counting_iterator<int> > ModuloCountingIterator;
		typedef transform_iterator<cdiv<int>, counting_iterator<int> > DivisionCountingIterator;

		DivisionCountingIterator keys = make_transform_iterator(make_counting_iterator(0), cdiv<int>(N));
		DivisionCountingIterator keys_end = keys + N*N;

		ModuloCountingIterator mKeys = make_transform_iterator(make_counting_iterator(0), cmod(N));
		ModuloCountingIterator mKeys_end = mKeys + N*N;

		thrust::transform(availabilities.begin(), availabilities.end(), distanceMatrix.begin(), sum.begin(), plus<float>());
		
		calculate_maxima(N, sup, (tuple<int, float, float>*)raw_pointer_cast(&row_maximas[0]));

		// Calculate Responsibilities
		thrust::transform(make_zip_iterator(make_tuple(first, make_permutation_iterator(row_maximas.begin(), mKeys))),
				make_zip_iterator(make_tuple(last, make_permutation_iterator(row_maximas.end(), mKeys))),
				responsibilities.begin(),
				calculate_responsibility(N, DAMPING_FACTOR, sp, rp, ap, sup)); 

		// Calculate sums over rows
		calculate_sums(N, rp, sup, (float*)raw_pointer_cast(&row_sums[0]));
		
		// Calculate Availabilities
		thrust::transform(first, last,
				availabilities.begin(),
				calculate_availability(N, DAMPING_FACTOR, sp, rp, ap, rmp));
		//	Calculate Exemplars
		bool changed = false;

		// First calculate sum of availabilities and responsibilities
		thrust::transform(responsibilities.begin(), responsibilities.end(), availabilities.begin(), sum.begin(), plus<float>());
		transpose(sup, N);

		// Find Exemplars
		reduce_by_key(keys, keys_end,
			make_zip_iterator(make_tuple(mKeys, sum.begin())),
			make_discard_iterator(),
			exemplars.begin(),
			equal_to<int>(),
			calculate_exemplars());
		pexemplars = hexemplars;
		hexemplars = exemplars;

		std::set<int> uniqueExemplars;
		
		for(int i = 0; i < N; i++) {

			if(get<0>(pexemplars[i]) != get<0>(hexemplars[i]))
				changed = true;
			uniqueExemplars.insert(get<0>(hexemplars[i]));

		}
		if (changed)
			nChanged = 0;
		else
			nChanged++;
		std::cout << "Num Changed: " << nChanged << " Num Exemplars: " << uniqueExemplars.size() << std::endl;

		nIter++;
	}

	/*host_vector<float> dM = distanceMatrix;
	host_vector<float> sM = sum;
	host_vector<float> rM = responsibilities;
	host_vector<float> aM = availabilities;*/
	/*std::cout << "Vectors: " << std::endl;
	for(int i = 0; i < N; i++) {
		float x = xd[i], y = yd[i], z = zd[i];
		printf("%d: (%.3f, %.3f, %.3f)\n", i+1, x, y, z);
	}*/
	/*std::cout << "Similarities: " << std::endl;
	print_matrix(N, dM);
	std::cout << "Responsibilities: " << std::endl;
	print_matrix(N, rM);
	std::cout << "Sums: " << std::endl;
	print_matrix(N, sM);
	std::cout << "Availabilities: " << std::endl;
	print_matrix(N, aM);*/

	//for(int i = 0; i < N*N; i++) {
	//	if(rM[i] > 0.0f ) {
	//		printf("Positive Resp (%d, %d): %8.4e\n", i%N, i/N, rM[i]);
	//	}
	//	if(aM[i] > 0.0f) {
	//		printf("Positive Avai (%d, %d): %8.4e\n", i%N, i/N, aM[i]);
	//	}
	//}
	output_pcd(N, x, y, z, exemplars);

	return 0;
}
